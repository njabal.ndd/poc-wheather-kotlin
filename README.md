# poc-wheather-kotlin

Réaliser une application universelle téléphone/tablette tout en
respectant les bonnes pratiques de développement sur Android.



Objectifs Réaliser une application universelle téléphone/tablette tout en respectant les bonnes pratiques de développement sur Android.
L’application comprend 2 écrans :
➢ Un écran principal avec la météo sur 5 jours de la ville de Paris
sous forme de liste
 ➢ Une vue « détail »

Au clic sur un élément de liste, la vue comprenant plus de détails sur l’élément sélectionné s’affiche (humidité, pression atmosphérique, température min/max., vitesse vent, etc.).
Il n’est pas nécessaire de créer une interface spécifique aux tablettes, mais il serait bien que le code soit prévu pour être facilement adaptable.
Ergonomie Inspirez-vous des guidelines Material Design

WebServiecs
API Météo http://openweathermap.org/api (REST/JSON)
- Météo à 5 jours pour Paris http://api.openweathermap.org/data/2.5/forecast/daily? q=Paris&units=metric&cnt=5&appid={appid}
-Vue détail : Météo détaillée pour la ville de Paris http://api.openweathermap.org/data/2.5/forecast/daily? q=Paris&appid={appid}
-Icônes http://openweathermap.org/weather-conditions
- AppId (pour authentifier les requêtes) : 8194ea842a9aef80a798c8ac0c320ec4


Consignes
- IDE : Android Studio
- L’utilisation de bibliothèques externes est autorisée et conseillée
- Version minimum : API 19 - Version target : API 27
- L’application doit pouvoir être utilisée en mode paysage/portrait
sans problèmes (hors UI)
 - Chaque fichier java doit comporter un header utilisant le
template suivant : /**
* Created by ${USER} on ${DATE}. */ public class MyClass { ...
- Le projet doit être archivé en respectant le format
{USER}.zip. {USER} correspond à votre NOM + « _ » + prénom - Nom du package de l’appli : com.{NAME}.weather où {NAME}
correspond à votre nom de famille (en minuscule, tout attaché)
