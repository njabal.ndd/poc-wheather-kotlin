package com.poc.weather.data.remote

import com.poc.weather.data.entities.Weather
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by njabal on 10/09/2018.
 */
interface WeatherApi {

    @GET("/data/2.5/forecast/daily")
    fun getWeather(@Query("q") city:String,
                   @Query("units")units:String,
                   @Query("cnt")cnt:Int,
                   @Query("appid")appid:String): Single<Weather>
}