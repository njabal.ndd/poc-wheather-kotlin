package com.poc.weather.data.entities
/**
 * Created by njabal on 10/09/2018.
 */
data class Temp (var day : Float,
                 var min : Float,
                 var max : Float,
                 var night : Float,
                 var eve : Float,
                 var morn : Float)