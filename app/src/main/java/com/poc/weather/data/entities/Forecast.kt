package com.poc.weather.data.entities
/**
 * Created by njabal on 10/09/2018.
 */
data class Forecast (var dt:Long,
                     var pressure:Float,
                     var humidity : Int,
                     var speed : Float,
                     var deg : Int,
                     var clouds : Int,
                     var temp : Temp,
                     var  weather : List<Description>
)
