package com.poc.weather.data.remote

import android.content.Context
import com.poc.weather.R
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
* Created by njabal on 10/09/2018.
*/
class Retrofit2Client {

    companion object {
        private var retrofit : Retrofit ? = null

        /**
         * Retrounrer le client retrofit (HTTP)
         * @param context
         * @return
         */
        fun getClient(context: Context):Retrofit?{
            if(retrofit == null){
                retrofit = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .baseUrl(context.getString(R.string.url_base))
                        .build()
            }
            return  retrofit
        }
    }
}