package com.poc.weather.data.entities

data class Description (var id : Int,
                   var main : String,
                   var description : String,
                   var icon : String)