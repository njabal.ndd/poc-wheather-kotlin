package com.poc.weather.data

import android.content.Context
import com.poc.weather.data.entities.Weather
import com.poc.weather.data.remote.Retrofit2Client
import com.poc.weather.data.remote.WeatherApi
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WeatherDataSource constructor(context: Context){

    private var weatherApi : WeatherApi?=null
    init {
        weatherApi = Retrofit2Client.getClient(context)?.create(WeatherApi::class.java)
    }

    fun loadForecasts(city:String,units:String,cnt:Int,appid:String,observer: SingleObserver<Weather>){
        var weatherObs = weatherApi?.getWeather(city,units,cnt,appid)!!

        weatherObs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer)
    }
}