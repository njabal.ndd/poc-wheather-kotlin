package com.poc.weather.ui.list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.poc.weather.R
import com.poc.weather.data.entities.Forecast
import com.poc.weather.mvp.adapter.WeatherAdapter
import com.poc.weather.mvp.list.ListPresenter
import com.poc.weather.mvp.list.ListPresenterImpl
import com.poc.weather.mvp.list.ListView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ListView {

    private var presenter : ListPresenter ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = ListPresenterImpl(this)
        presenter!!.attachView(this)

        presenter!!.loadForecasts(getString(R.string.ws_city),getString(R.string.ws_unit), resources.getInteger(R.integer.ws_cnt),getString(R.string.ws_appid))
    }





    override fun showProgress() {
        act_main_progress?.visibility = View.VISIBLE
        act_main_list_weather?.visibility = View.GONE
    }

    override fun hideProgress() {
        act_main_progress?.visibility = View.GONE
        act_main_list_weather?.visibility = View.VISIBLE
    }

    override fun onSuccessLoadForecast(list: List<Forecast>) {
        act_main_txt_hello?.setText(getString(R.string.app_name) + " =+> "+list?.size)
        act_main_list_weather?.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        var adapter = WeatherAdapter(list)
        act_main_list_weather?.adapter = adapter

    }

    override fun onErrorLoadForecast(thorwable: Throwable) {
        Toast.makeText(this,"Error ==> ",Toast.LENGTH_SHORT).show()
    }


}
