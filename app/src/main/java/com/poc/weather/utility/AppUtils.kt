package com.poc.weather.utility

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class AppUtils {

    companion object {
        fun stringtoDate(dates: String): Date {
            val sdf = SimpleDateFormat("EEE, MMM dd yyyy",
                    Locale.ENGLISH)
            var date: Date? = null
            try {
                date = sdf.parse(dates)
                println(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return date!!
        }

        fun timeStamptoString(timeStamp: Long): String? {
            try {
                val sdf = SimpleDateFormat("EEEE dd/MM/yyyy")
                val netDate = Date(timeStamp* 1000)
                return sdf.format(netDate)
            } catch (e: Exception) {
                return e.toString()
            }
        }
    }

}