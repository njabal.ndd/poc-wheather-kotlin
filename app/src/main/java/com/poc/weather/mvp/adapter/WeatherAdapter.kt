package com.poc.weather.mvp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.poc.weather.R
import com.poc.weather.data.entities.Forecast
import com.poc.weather.utility.AppUtils

class WeatherAdapter(val foreCastList: List<Forecast>): RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.txtDate?.text = AppUtils.timeStamptoString(foreCastList[position].dt)
        println(foreCastList[position].dt)
        holder?.txtdesc?.text = foreCastList[position]?.weather[0]?.description

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.item_weathe_layout, parent, false)
        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return foreCastList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txtDate = itemView.findViewById<TextView>(R.id.adap_txt_date)
        val txtdesc = itemView.findViewById<TextView>(R.id.adap_txt_weather_desc)

    }

}