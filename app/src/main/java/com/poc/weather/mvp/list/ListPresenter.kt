package com.poc.weather.mvp.list
/**
 * Created by njabal on 10/09/2018.
 */
interface ListPresenter {

    fun attachView(v : ListView)

    fun loadForecasts(city:String,units:String,cnt:Int,appid:String)
}