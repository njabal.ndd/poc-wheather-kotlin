package com.poc.weather.mvp.list

import com.poc.weather.data.entities.Forecast
/**
 * Created by njabal on 10/09/2018.
 */
interface ListView {
    /**
     * Show progress when treat operation
     */
    fun showProgress()
    /**
     * Hide progress after treatment
     */
    fun hideProgress()
    /**
     * when WS return with success
     */
    fun onSuccessLoadForecast(list : List<Forecast>)
    /**
     * when WS return with Error
     */
    fun onErrorLoadForecast(thorwable : Throwable)
}